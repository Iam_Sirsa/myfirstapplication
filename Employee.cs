﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCApplication.Models
{
    public class Employee
    {
        [Key]
        public int Empid { get; set; }

        public string name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string confirmpassword { get; set; }

        public string gender { get; set; }//for radio button
        public DateTime dob { get; set; }
        [DisplayName("upload File")]
        public string image { get; set; }//for photo
        [NotMapped]
        public HttpPostedFileBase Imagefile { get; set; }
        public City choosecity { get; set; }
        public enum City
        {
            soro = 1,
            balasore = 2
        }
    }
}